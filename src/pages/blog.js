import React from "react"
import { graphql, useStaticQuery, Link } from "gatsby"
import Layout from "../component/Layout"

const Blog = () => {
  const Contentful = useStaticQuery(graphql`
    query {
      allContentfulCourse {
        edges {
          node {
            id
            title
            slug
            createdAt(formatString: "DD MMMM, YYYY")
          }
        }
      }
    }
  `)
  return (
    <div>
      <Layout>
        {" "}
        <div>
          
          <h1>Blogs</h1>
          <h2></h2>
          <ul>
            {Contentful.allContentfulCourse.edges.map(edge => (
              <div>
                <li className={"c"}>
                  <Link to={`/blog/${edge.node.slug}`}>{edge.node.title}</Link>
                </li>
                <p>Published Date : {edge.node.createdAt}</p>
                <p></p>
              </div>
            ))}
          </ul>
        </div>{" "}
      </Layout>{" "}
    </div>
  )
}
export default Blog
