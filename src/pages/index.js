import React from "react"
import Layout from "../component/Layout"
import Img from "gatsby-image"
import {useStaticQuery} from "gatsby"




const Home=()=>{

  const data = useStaticQuery(graphql`
  query {
    

      file(relativePath: { eq: "images/ss.jpg" }) {
        childImageSharp {
          fixed(width: 350, height: 250) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
    
  
`)

  return <div>
    <Layout>
      
      <h3><b>A Big yes to Work From Home</b></h3>
      <Img
        fixed={data.file.childImageSharp.fixed}
        alt="Gatsby Docs are awesome"
      />
      <p>  We are working from home since past 5 Months due to the COVID19 pandemic. And I won't say it's bad or great. We are getting used to it slowly but gradually. Most of the things are going well. We are able to run all the meetings online with the help of some tools and apps.</p>
       Working from home is experiencing a real increase in uptake throughout the world. More people want the flexibility, comfort and practicality of not needing to travel to the office every day. If you’re interested in how it all works, there are a number of online bloggers who detail the ins and outs of working from your own home. We’ve selected some of the best to help you on your way towards realising your remote work dreams!
       </Layout></div>

}

export default  Home